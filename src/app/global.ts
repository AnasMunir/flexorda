import { Injectable } from '@angular/core'
import { FormGroup } from '@angular/forms';

@Injectable()
export class GlobalVariable {
//   public url: string = "http://139.59.112.103:3000/";
  public current_userUID: string = "";
  public notificationToken: string = "";
  public lat;
  public lng;
  public cacheProducts: FormGroup;
}