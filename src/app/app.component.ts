import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
// import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { GlobalVariable } from './global';
import { OneSignal } from '@ionic-native/onesignal';
import { FirebaseService } from "../providers/firebase-service";

import { Push, PushToken } from '@ionic/cloud-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any /*= 'Landing'*/;

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    geolocation: Geolocation,
    globals: GlobalVariable,
    private push: Push,
    private afAuth: AngularFireAuth,
    private fs: FirebaseService,
    private oneSignal: OneSignal) {

    const authObserver = afAuth.authState.subscribe(async (user) => {
      if (user) {
        globals.current_userUID = user.uid;
        this.rootPage = 'Tabs';
        // const token = await this.initializePushToken(user.uid);
        // console.log('Token saved:',token);
        // this.fs.updatePushToken(token);
        authObserver.unsubscribe();
      } else {
        this.rootPage = 'Landing';
        authObserver.unsubscribe();
      }
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // this.initializePushToken()
      //   .then(() => {
      //     this.push.rx.notification()
      //       .subscribe((msg) => {
      //         console.log(msg);
      //         alert(msg.title + ': ' + msg.text);
      //       });
      //   })
      geolocation.getCurrentPosition().then((resp) => {
        globals.lat = resp.coords.latitude;
        globals.lng = resp.coords.longitude;
        console.log(resp.coords.latitude);
        console.log(resp.coords.longitude)
      }, error => {
        console.error('error getting location: ' + error);
      })

      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  async initializePushToken(userUid) {
    const token: PushToken = await this.push.register()
    this.push.saveToken(token);
    return token.token;
    /*.then((t: PushToken) => {
      return this.push.saveToken(t);
    })*/
    /*.then((t: PushToken) => {
      console.log('Token saved:', t.token);
    });*/
  }
}

