import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { CloudSettings, CloudModule } from "@ionic/cloud-angular";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from "@ionic-native/camera";
import { Geolocation } from "@ionic-native/geolocation";
import { ImagePicker } from "@ionic-native/image-picker";
import { NativeStorage } from "@ionic-native/native-storage";
import { OneSignal } from '@ionic-native/onesignal';

// importing providers
import { AuthService } from '../providers/auth-service';
import { FirebaseService } from '../providers/firebase-service';
import { PushService } from "../providers/push-service";
import { GoogleMaps } from '../providers/google-maps';
import { GlobalVariable } from './global';

// importing angularire 2 
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from "angularfire2/auth";

export const firebaseConfig = {
  apiKey: "AIzaSyB-Dpy7JHP2a9XvU4DMNGSonbC2QQ5vZ9E",
  authDomain: "flex-orda.firebaseapp.com",
  databaseURL: "https://flex-orda.firebaseio.com",
  projectId: "flex-orda",
  storageBucket: "flex-orda.appspot.com",
  messagingSenderId: "206080062764"
};

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': 'c8dcfea9'
  },
  'push': {
    'sender_id': '189387222423',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#343434'
      }
    }
  }
};
// Configuring Firebase auth
// const myFirebaseAuthConfig = {
//   provider: AuthProviders.Password,
//   method: AuthMethods.Password
// }

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
    }),
    CloudModule.forRoot(cloudSettings),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    Camera,
    ImagePicker,
    NativeStorage,
    OneSignal,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    GlobalVariable,
    AuthService,
    FirebaseService,
    PushService,
    GoogleMaps
  ]
})
export class AppModule { }
