import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderDropoff } from './order-dropoff';

@NgModule({
  declarations: [
    OrderDropoff,
  ],
  imports: [
    IonicPageModule.forChild(OrderDropoff),
  ],
  exports: [
    OrderDropoff
  ]
})
export class OrderDropoffModule {}
