import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NativeStorage } from "@ionic-native/native-storage";

import { GoogleMaps } from '../../providers/google-maps';
import { PlaceModel } from '../../models/place-model';
import { FirebaseService } from '../../providers/firebase-service';
import { GlobalVariable } from "../../app/global";
import { Geolocation } from "@ionic-native/geolocation";
declare var google;

@IonicPage()
@Component({
  selector: 'page-order-dropoff',
  templateUrl: 'order-dropoff.html',
})
export class OrderDropoff implements OnInit {

  submitAttempt: boolean = false;
  products: any;
  orderPickupDetails: any;
  shopDetails: any;
  element: HTMLElement;
  dropoffPicture: any;
  deliveryTimeType: any;
  timeType: string = "anyTime";
  currentDateTime: any;
  currentHours: any;
  currentMinutes: any;
  timmingType: string = 'anyTime';
  checked: boolean = true;
  clearInput: boolean = true;
  clearOnEdit: boolean = true;
  autocompleteItems: any;
  autocomplete: any;
  orderForm: FormGroup;
  totalEstimate: any;
  dropoffAddressDetails = {
    formatted_address: '',
    name: '',
    placeId: '',
    lat: '',
    lng: '',
  }
  currentDay; currentMonth; currentYear;
  addressDetails = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public gm: GoogleMaps,
    private nativeStorage: NativeStorage,
    private globals: GlobalVariable,
    private geolocation: Geolocation,
    public fs: FirebaseService) {

    this.shopDetails = navParams.get("shopDetails");
    this.orderPickupDetails = navParams.get("orderPickupDetails");
    this.products = navParams.get("products");
    this.totalEstimate = navParams.get('totalEstimate');

    let currentDateTime = new Date();

    this.currentYear = currentDateTime.getFullYear().toString();
    this.currentMonth = currentDateTime.getMonth() + 1;
    this.currentDay = currentDateTime.getDate().toString();

    if (this.currentMonth.toString().length === 1) {
      this.currentMonth = "0" + this.currentMonth;
    }
    if (this.currentDay.length === 1) {
      this.currentDay = "0" + this.currentDay;
    }
    let currentHours = currentDateTime.getHours().toString();
    if (currentHours.length === 1) {
      this.currentHours = "0" + currentHours;
    } else {
      this.currentHours = currentHours;
    }
    console.log('this.currentHours', this.currentHours);

    let currentMinutes = currentDateTime.getMinutes().toString();
    if (currentMinutes.length === 1) {
      this.currentMinutes = "0" + currentMinutes;
    } else {
      this.currentMinutes = currentMinutes;
    }
    console.log("this.currentMinutes", this.currentMinutes);

    this.deliveryTimeType = new FormGroup({
      "deliveryTimeType": new FormControl({ value: 'anytime', disabled: false })
    });

    this.orderForm = formBuilder.group({
      // time: ['', Validators.required],
      reward: ['', Validators.required],
      // anyTime: [currentDateTime.toISOString(), Validators.required],
      anyTime: ['',],
      betweenTimeOne: ['',],
      betweenTimeTwo: ['',]
      // betweenTimeOne: [{ value: 'choose time', disabled: true }],
      // betweenTimeTwo: [{ value: 'choose time', disabled: true }]
    });
  }

  ngOnInit() {
    this.element = document.getElementById('map');
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
    this.nativeStorage.getItem("addresses")
      .then(
      (data) => this.addressDetails = data.addresses
      )
      .catch(
      () => this.addressDetails = []
      )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderDropoff');
  }

  radioSelect($event) {
    console.log('radio button selected')
    console.log($event);
    this.timeType = $event;
    if ($event === 'specificTime') {
      this.orderForm.get('betweenTimeOne').enable();
      this.orderForm.get('betweenTimeTwo').enable();
      this.orderForm.get('anyTime').disable();
    } else if ($event === 'anyTime') {
      this.orderForm.get('betweenTimeOne').disable();
      this.orderForm.get('betweenTimeTwo').disable();
      this.orderForm.get('anyTime').enable();
    }
  }

  search() {
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    var pyrmont = new google.maps.LatLng(this.globals.lat, this.globals.lng);
    let request = {
      location: pyrmont,
      radius: '5000',
      query: this.autocomplete.query,
    }
    let self = this;
    let service = new google.maps.places.PlacesService(this.element);
    service.textSearch(request, (results, status) => {
      console.log(status);
      if (status == 'OK') {
        this.autocompleteItems = results;

      }
      console.log(results);
    })
  }

  chooseItem(item) {
    this.autocompleteItems = [];
    this.autocomplete.query = item.name + " " + item.formatted_address;
    console.log(item);
    this.dropoffAddressDetails.formatted_address = item.formatted_address;
    this.dropoffAddressDetails.name = item.name;
    this.dropoffAddressDetails.lat = item.geometry.location.lat();
    this.dropoffAddressDetails.lng = item.geometry.location.lng();
    this.dropoffAddressDetails.placeId = item.place_id;
    if (item.photos) {
      this.dropoffPicture = item.photos[0].getUrl({ 'maxWidth': 300, 'maxHeight': 150 });
    }
  }
  async addOrder() {
    this.submitAttempt = true;
    if (!this.orderForm.valid || this.dropoffAddressDetails.name == '') {
      console.log("no ways");
    } else {
      this.initiateLoader()
      console.log(this.orderForm.value);
      console.log(this.shopDetails);
      // console.log(this.orderPickupDetails);
      console.log(this.products);
      console.log(this.dropoffAddressDetails);
      await this.fs.addOrder(
        this.totalEstimate,
        this.shopDetails,
        this.dropoffAddressDetails,
        this.orderForm.value,
        // this.orderPickupDetails,
        this.products
      );
      this.navCtrl.setRoot("ActiveOrders");
    }
  }
  initiateLoader() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
  }

  setCurrentLocation() {
    console.log("yo wattup");
    var geocoder = new google.maps.Geocoder;
    this.geolocation.getCurrentPosition()
      .then((resp) => {
        let latlng = { lat: resp.coords.latitude, lng: resp.coords.longitude }
        geocoder.geocode({ 'location': latlng }, (results, status) => {
          console.log(status);
          console.log(results);
          if (status == 'OK') {
            this.getPlaceDetails(results[0].place_id);
          }
        }, error => {
          console.error('error getting location: ' + error);
        });
      })

  }
  getPlaceDetails(place_id: string) {
    let request = {
      placeId: place_id
    };
    console.log("details for place id: ", place_id);
    let service = new google.maps.places.PlacesService(this.element);
    service.getDetails(request, (place, status) => {
      console.log(status);
      console.log("placae details");
      console.log(place);
      this.dropoffAddressDetails.formatted_address = place.formatted_address;
      this.dropoffAddressDetails.name = place.name;
      this.dropoffAddressDetails.lat = place.geometry.location.lat();
      this.dropoffAddressDetails.lng = place.geometry.location.lng();
      this.dropoffAddressDetails.placeId = place.place_id;
      console.log(this.dropoffAddressDetails);
      this.autocomplete.query = place.name + ' ' + place.formatted_address
    })
  }

  showRadio() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Saved Addresses');
    this.addressDetails.map(address => {
      alert.addInput({
        type: 'radio',
        label: address.nickname,
        value: address.placeId
      });
    })

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        console.log(data);
        this.addressDetails.map(address => {
          if (data == address.placeId) {
            this.dropoffAddressDetails.formatted_address = address.formatted_address;
            this.dropoffAddressDetails.name = address.name;
            this.dropoffAddressDetails.lat = address.lat;
            this.dropoffAddressDetails.lng = address.lng;
            this.dropoffAddressDetails.placeId = address.placeId;
            console.log(this.dropoffAddressDetails);
            this.autocomplete.query = address.name + ' ' + address.formatted_address
          }
        })
      }
    });
    if (this.addressDetails.length > 0) {
      alert.present();
    }
  }
}
