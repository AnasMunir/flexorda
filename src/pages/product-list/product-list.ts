import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ImagePicker, ImagePickerOptions } from "@ionic-native/image-picker";
import { Camera, CameraOptions } from '@ionic-native/camera';

@IonicPage()
@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html',
})
export class ProductList {
  submitAttempt: boolean = false;
  products: Array<{ name: string, cost: number, images: string[] }> = [];
  shopDetails: any;

  productName: string = '';
  productCost: string = '0';
  productImages: Array<string> = [];
  totalEstimate: number = 0;
  cameraOptions: CameraOptions = {
    quality: 95,
    destinationType: this.camera.DestinationType.DATA_URL,
    sourceType: this.camera.PictureSourceType.CAMERA,
    encodingType: this.camera.EncodingType.PNG,
    targetWidth: 500,
    targetHeight: 500,
    saveToPhotoAlbum: true
  };
  pickerOptions: ImagePickerOptions = {
    width: 500,
    height: 500,
    quality: 95,
    outputType: 1,
    maximumImagesCount: 5
  };
  clearOnEdit: boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private imagePicker: ImagePicker,
    private camera: Camera) {

    this.shopDetails = navParams.get('shopDetails');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductList');
  }

  async pickImage() {
    try {
      var images = await this.imagePicker.getPictures(this.pickerOptions)
      console.log(images);
      images.map(image => {
        this.productImages.push(image);
      });
      console.log("productImages after mapping:", this.productImages);
    } catch (error) {
      console.error("error uploading image: ", error)
    }
  }

  async openCamera() {
    try {
      const image = await this.camera.getPicture(this.cameraOptions);
      this.productImages.push(image);
      console.log("productImages:", this.productImages);
    } catch (error) {
      console.error("error uploading image: ", error)
    }
  }

  addProduct(name, cost, index: number) {
    console.log(name + ' ' + cost + ' ' + index);
    this.products.push({ name: name, cost: parseInt(cost), images: this.productImages });
    this.totalEstimate += parseInt(cost);
    // this.products[index].name = name;
    // this.products[index].cost = cost;
    // this.products[index].images = this.productImages;
    this.initilizeVariables();
  }

  initilizeVariables() {
    this.productName = '';
    this.productCost = '0';
    this.productImages = [];

  }

  removeProduct(index: number) {
    console.log(this.products);
    console.log(index);
    this.totalEstimate -= this.products[index].cost;
    this.products.splice(index, 1);
    console.log(this.products);
  }

  proceed() {
    if (this.products.length < 1) {
      console.log("no ways");
    } else {
      this.navCtrl.push("OrderDropoff",
        {
          products: this.products,
          shopDetails: this.shopDetails,
          totalEstimate: this.totalEstimate
        });
    }
  }

}
