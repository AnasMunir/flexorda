import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Landing page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class Landing {

  slides = [
    {
      title: "Welcome to the World!",
      description: "Lorem ipsum",
      image: "assets/img/logo.png",
    },
    {
      title: "Welcome to the World!",
      description: "Lorem ipsum",
      image: "assets/img/logo.png",
    },
    {
      title: "Welcome to the World!",
      description: "Lorem ipsum",
      image: "assets/img/logo.png",
    }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Landing');
  }

  goToSignup() {
    this.navCtrl.push('Register');
  }

  goToLogin() {
    this.navCtrl.push('Login');
  }

}
