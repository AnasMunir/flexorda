import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FirebaseListObservable } from 'angularfire2/database';

import { FirebaseService } from '../../providers/firebase-service';

@IonicPage()
@Component({
  selector: 'page-my-orders',
  templateUrl: 'my-orders.html',
})
export class MyOrders {

  userOrders$: FirebaseListObservable<any>;
  orderStatus: string = "active";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public fs: FirebaseService) {

    let orderStatus = navParams.get('orderStatus');
    if (orderStatus) {
      this.orderStatus = orderStatus;
    }

    this.getUserOrders();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyOrders');
  }

  getUserOrders() {
    this.userOrders$ = this.fs.getUserOrders();
  }
  deleteOrder(orderKey) {
    let status = this.fs.deleteOrder(orderKey);
    status.then(data => {
      console.log(data);
      console.log('order deleted');
    }, (error) => {
      console.error('Error deleting order: ' + error);
    })
  }

  async editOrder(orderUID) {
    await this.fs.updateOrderStatus(orderUID, 'in_active');
    let editModal = this.modalCtrl.create('EditModal',
      {
        orderUID: orderUID
      });
    editModal.present();
  }

}
