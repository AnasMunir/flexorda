import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditModal } from './edit-modal';

@NgModule({
  declarations: [
    EditModal,
  ],
  imports: [
    IonicPageModule.forChild(EditModal),
  ],
  exports: [
    EditModal
  ]
})
export class EditModalModule {}
