import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FirebaseListObservable } from 'angularfire2/database';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
// importing provider
import { FirebaseService } from '../../providers/firebase-service';

@IonicPage()
@Component({
  selector: 'page-edit-modal',
  templateUrl: 'edit-modal.html',
})
export class EditModal {
  userOrder$: FirebaseListObservable<any>;
  orderUID: any;
  items = [];
  orderForm: FormGroup;
  initProd: any;
  shopName: any;
  cost: any;
  delivery_address: any;
  time: any;
  reward: any;
  deliveryTimeType: any;
  anyTime: any;
  betweenTimeOne: any;
  betweenTimeTwo: any;
  products: Array<{ name: string, cost: number, images: string[] }> = [];
  data: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    public fs: FirebaseService) {

    this.orderUID = navParams.get('orderUID');
    console.log(this.orderUID);
    this.getOrder(this.orderUID);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditModal');
  }

  dismiss() {
    this.fs.updateOrderStatus(this.orderUID, 'active')
      .then(() => {
        this.viewCtrl.dismiss();
      })
  }

  async getOrder(orderUID) {

    const item = await this.fs.getSpecificOrder(orderUID);
    this.data = item.val();
    console.log(item.val());
    this.reward = item.val().reward;
    if (item.val().delivery_time.anyTime) {
      this.deliveryTimeType = 'anyTime';
      this.anyTime = item.val().delivery_time.anyTime;
    }
    if (item.val().delivery_time.betweenTimeOne) {
      this.deliveryTimeType = 'betweenTimeOne';
      this.betweenTimeOne = item.val().delivery_time.betweenTimeOne;
      this.betweenTimeTwo = item.val().delivery_time.betweenTimeTwo;
    }
    // item.val().products.map(product => {

    // })
    this.products = item.val().products
    // this.fs.getSpecificOrder(orderUID).then(snapshot => {
    //   this.items.push(snapshot.val());
    //   // this.items = snapshot.val();
    //   console.log(this.items);
    //   this.shopName = this.items[0].shop.shop_name;
    //   this.cost = snapshot.val().cost;
    //   this.time = this.items[0].time;
    //   this.delivery_address = this.items[0].delivery_address.address;
    //   this.reward = this.items[0].reward;
    //   console.log(this.orderForm.value)
    // })
  }
  async saveChanges() {
    var editData;
    console.log(this.products);
    if (this.deliveryTimeType == 'anyTime') {
      editData = {
        reward: this.reward,
        deliveryTime: this.anyTime
      }
    }
    if (this.deliveryTimeType == 'betweenTimeOne') {
      editData = {
        reward: this.reward,
        betweenTimeOne: this.betweenTimeOne,
        betweenTimeTwo: this.betweenTimeTwo
      }
    }
    await this.fs.editOrder(this.orderUID, editData, this.products, this.deliveryTimeType);
    await this.fs.updateOrderStatus(this.orderUID, 'active')
    this.viewCtrl.dismiss();
  }
}
