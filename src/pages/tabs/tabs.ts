import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class Tabs {

  tab1Root: any = 'ActiveOrders';
  tab2Root: any = 'MyOrders';
  tab3Root: any = 'OrderPickup';
  tab4Root: any = 'MyDeliveries';
  tab5Root: any = 'Profile';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Tabs');
  }

}
