import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, AlertController, ModalController } from 'ionic-angular';

import { GoogleMaps } from '../../providers/google-maps';
import { FirebaseService } from '../../providers/firebase-service';
import { PlaceModel } from '../../models/place-model';
import { GlobalVariable } from "../../app/global";
declare var google;

@IonicPage()
@Component({
  selector: 'page-order-pickup',
  templateUrl: 'order-pickup.html',
})
export class OrderPickup implements OnInit {

  submitAttempt: boolean = false;
  autocompleteItems: any;
  autocomplete: any;
  searchAttempt: boolean = false;
  element: HTMLElement;
  shopDetails = {
    formatted_address: '',
    name: '',
    placeId: '',
    lat: '',
    lng: '',
    opening_hours: ''
  }
  shopPicture: any;
  openNow: boolean;
  currentDay: any;
  opening_hours: boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    private globals: GlobalVariable,
    public gm: GoogleMaps,
    public fs: FirebaseService) {

    let date = new Date();
    this.currentDay = date.getDay();
  }
  ngOnInit() {
    this.element = document.getElementById('map');
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderPickup');

  }
  ionViewDidEnter() {
    this.fs.watchUserLocation();

  }

  search() {
    console.log("autocomplete query: ", this.autocomplete.query);
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }

    var pyrmont = new google.maps.LatLng(this.globals.lat, this.globals.lng);
    let request = {
      location: pyrmont,
      radius: '5000',
      query: this.autocomplete.query,
    }
    let self = this;
    let service = new google.maps.places.PlacesService(this.element);
    service.textSearch(request, (results, status) => {
      console.log(status);
      if (status == 'OK') {
        this.autocompleteItems = results;

      }
      console.log(results);
    })
  }
  chooseItem(item) {
    this.searchAttempt = true;
    this.autocompleteItems = [];
    this.autocomplete.query = item.name + " " + item.formatted_address;
    console.log(item);
    this.shopDetails.formatted_address = item.formatted_address;
    this.shopDetails.name = item.name;
    this.shopDetails.lat = item.geometry.location.lat();
    this.shopDetails.lng = item.geometry.location.lng();
    this.shopDetails.placeId = item.place_id;
    if(item.opening_hours) {
      this.opening_hours = true;
    } else {
      this.opening_hours = false;
    }
    this.getPlaceDetails(item.place_id);
    this.shopPicture = '';
    if (item.photos) {
      this.shopPicture = item.photos[0].getUrl({ 'maxWidth': 300, 'maxHeight': 150 });
    }
    if (item.opening_hours) {
      this.openNow = true;
    } else {
      this.openNow = false;
    }
    console.log(this.shopPicture);

  }
  addProducts() {
    // let modal = this.modalCtrl.create("ShoppingList");
    // modal.present();
    // modal.onDidDismiss(data => {
    //   if (data) {
    //     this.products = data;
    //     console.log(this.products);
    //   } else {
    //     console.log("modal was cancelled");
    //   }
    // })
    this.submitAttempt = true;
    if (this.shopDetails.name == '' || this.shopDetails.formatted_address == '') {
      console.log("no ways");
    } else {
      this.navCtrl.push("ProductList", { shopDetails: this.shopDetails })
    }
  }

  getPlaceDetails(place_id: string) {
    let request = {
      placeId: place_id
    };
    console.log("details for place id: ", place_id);
    let service = new google.maps.places.PlacesService(this.element);
    service.getDetails(request, (place, status) => {
      console.log(status);
      console.log("placae details");
      console.log(place);
      // console.log(place.photos[0].getUrl({ 'maxWidth': 200, 'maxHeight': 200 }));
      if (place.opening_hours) {
        this.shopDetails.opening_hours = place.opening_hours.weekday_text;
      }
    })
  }
}
