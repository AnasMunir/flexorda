import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderPickup } from './order-pickup';

@NgModule({
  declarations: [
    OrderPickup,
  ],
  imports: [
    IonicPageModule.forChild(OrderPickup),
  ],
  exports: [
    OrderPickup
  ]
})
export class OrderPickupModule {}
