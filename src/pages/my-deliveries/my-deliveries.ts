import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirebaseService } from '../../providers/firebase-service';
import { FirebaseListObservable } from 'angularfire2/database';

@IonicPage()
@Component({
  selector: 'page-my-deliveries',
  templateUrl: 'my-deliveries.html',
})
export class MyDeliveries {
  userDeliveries$: FirebaseListObservable<any>;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fs: FirebaseService) {

    this.getUserDeliveries();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Deliveries');
  }
  
  viewDetails(order) {
    this.navCtrl.push('OrderDetails', {
      order: order
    });
  }
  getUserDeliveries() {
    this.userDeliveries$ = this.fs.getUserDeliveries();
  }
}
