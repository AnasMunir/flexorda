import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyDeliveries } from './my-deliveries';

@NgModule({
  declarations: [
    MyDeliveries,
  ],
  imports: [
    IonicPageModule.forChild(MyDeliveries),
  ],
  exports: [
    MyDeliveries
  ]
})
export class MyDeliveriesModule {}
