import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../../providers/auth-service';
import { FirebaseService } from "../../providers/firebase-service";
import { PushService } from "../../providers/push-service";
import { EmailValidator } from '../../validators/email';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {

  public submitAttempt: boolean = false;
  public loginForm: FormGroup;
  loader: any;

  constructor(public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private auth: AuthService,
    private fs: FirebaseService,
    private ps: PushService, ) {

    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  async login() {
    this.loader = this.loadingCtrl.create({
      content: "Loggin In...",
      dismissOnPageChange: true
    });
    this.submitAttempt = true;
    if (!this.loginForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      try {
        this.loader.present();
        console.log('success!');
        console.log(this.loginForm.value);
        const user = await this.auth.loginUser(
          this.loginForm.value.email,
          this.loginForm.value.password, )
        this.navCtrl.setRoot('Tabs');
        // const token = await this.ps.initializePushToken(user.uid);
        // console.log("logging token " + token + " from login.ts");
        // this.fs.updatePushToken(token);
      } catch (error) {
        this.loginAlert(error);
      }
    }
  }

  loginAlert(error) {
    this.loader.dismiss();
    let alert = this.alertCtrl.create({
      title: "Login error",
      subTitle: error,
      buttons: ["OK"]
    })
    alert.present();
  }
  goToSignup() {
    // this.navCtrl.setRoot('Register');
    this.navCtrl.push('Register')
  }

}
