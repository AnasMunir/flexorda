import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ModalController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class Profile {

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private as: AuthService,
    public modalCtrl: ModalController,
    public appCtrl: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Profile');
  }

  async logOut() {
    await this.as.logoutUser();
    this.appCtrl.getRootNav().setRoot('Landing');
  }
  addAddresses() {
    // let modal = this.modalCtrl.create("AddressList");
    // modal.present();
    this.navCtrl.push("AddressList");
  }

}
