import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-order-details',
  templateUrl: 'order-details.html',
})
export class OrderDetails {
  order: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.order = navParams.get("order");
    console.log(this.order);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderDetails');
  }

}
