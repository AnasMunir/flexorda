import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActiveOrders } from './active-orders';

@NgModule({
  declarations: [
    ActiveOrders,
  ],
  imports: [
    IonicPageModule.forChild(ActiveOrders),
  ],
  exports: [
    ActiveOrders
  ]
})
export class ActiveOrdersModule {}
