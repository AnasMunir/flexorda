import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import firebase from 'firebase';
import { FirebaseService } from '../../providers/firebase-service';

declare var GeoFire: any;

@IonicPage()
@Component({
  selector: 'page-active-orders',
  templateUrl: 'active-orders.html',
})
export class ActiveOrders {

  loader: any;
  items: any;
  orders: Array<any> = [];
  orderRef: any;
  currentTime: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public fs: FirebaseService,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private zone: NgZone,
    /*public af: AngularFire*/) {
    this.orderRef = firebase.database().ref("orders/");
    // this.getActiveOrders();
    let today = new Date();
    this.currentTime = today.getHours() + ':' + today.getMinutes();
    this.getNearbyOrders();

    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 5000
    });
    this.loader.present();
  }

  viewDetails(order) {
    this.navCtrl.push('OrderDetails', {
      order: order
    });
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.navCtrl.setRoot("ActiveOrders");
    setTimeout(() => {
      console.log('Async operation has ended');
      // this.navCtrl.setRoot("ActiveOrders");
      refresher.complete();
    }, 2000);
  }

  async deliver(orderId, buyerId, item) {
    this.orders.map((item, i, array) => {
      if (orderId == item.uid) {
        array.splice(i, 1)
      }
    })
    let alert = this.alertCtrl.create({
      title: 'Deliveries Updated',
      subTitle: 'This order is placed to delivery list',
      buttons: ['Enjoy Shopping']
    });

    this.fs.updateOrderStatus(orderId, "in_process");
    this.fs.updateOrderStatusInBuyer(buyerId, orderId, "in_process");
    this.fs.addDeliveriesInCurrentUser(buyerId, orderId, item);
    alert.present();
    alert.onDidDismiss(
      () => this.navCtrl.setRoot("MyOrders", { orderStatus: 'in_process' })
    )
    console.log(this.orders);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActiveOrders');
    // this.getNearbyOrders();
  }

  ngAfterViewInit() {
    // this.getNearbyOrders();
  }

  async getNearbyOrders() {
    const pos = await this.fs.getCurrentPosition();
    console.log('lat: ' + pos.coords.latitude + ', lng: ' + pos.coords.longitude);
    await this.fs.setUserLocation(pos.coords.latitude, pos.coords.longitude);
    let ref = firebase.database().ref('orders/locations');
    let geoFire = new GeoFire(ref);

    let geoQuery = geoFire.query({
      center: [pos.coords.latitude, pos.coords.longitude],
      radius: 10
    });
    // this.orders = [];
    geoQuery.on("key_entered", key => this.ordersInZone(key));
    geoQuery.on("key_exited", key => this.deleteOrders(key));
  }

  ordersInZone(key) {
    this.zone.run(
      () => this.insertOrders(key)
    )
  }
  async insertOrders(key) {
    try {

      const snapshot = await this.orderRef.once('value');
      console.log("logging to see: ", snapshot.val()[key]);
      if (snapshot.val()[key] !== undefined) {

        this.orders.push(snapshot.val()[key]);
        // if (snapshot.val()[key].delivery_time.anyTime) {

        //   if (this.currentTime < snapshot.val()[key].delivery_time.anyTime) {
        //     this.orders.push(snapshot.val()[key]);
        //   }
        // }
        // if (snapshot.val()[key].delivery_time.betweenTimeOne) {

        //   if (this.currentTime < snapshot.val()[key].delivery_time.betweenTimeTwo) {
        //     this.orders.push(snapshot.val()[key]);
        //   }
        // }
        if (this.orders.length > 0) {
          this.loader.dismiss();
        }
        // if (this.orders.length > 0) {
        //   this.orders.map(
        //     (item) => {
        //       console.log('items');
        //       console.log(item);
        //       if (item["uid"] === snapshot[key].uid) {
        //         console.log("key already in orders");
        //       } else {
        //         // let object = { [key]: snapshot[key] }
        //         this.orders.push(snapshot.val()[key]);
        //       }
        //     })
        // } else {
        // }
      }
    } catch (error) {
      console.error("error in insert Orders: ", error);
    }
    console.log(this.orders);
  }

  async deleteOrders(key) {
    const snapshot = await this.orderRef.once('value');
    console.log("logging to see: ", snapshot.val()[key]);
    let index = this.orders.findIndex(item => {
      return item.shop.place_id === snapshot.val()[key].shop.place_id;
    });
    this.orders.splice(index, 1);
    console.log("orders updated on key exit", key);
    console.log(this.orders);
  }

}
