import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../providers/auth-service';
import { FirebaseService } from "../../providers/firebase-service";
import { PushService } from "../../providers/push-service";

// import { GlobalVariable } from "../../app/global";
import { EmailValidator } from '../../validators/email';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class Register {

  submitAttempt: boolean = false;
  signupForm: FormGroup;
  currentYear: any;
  loader: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private auth: AuthService,
    private fs: FirebaseService,
    private ps: PushService,
    /*private globals: GlobalVariable*/) {

    let date = new Date();
    this.currentYear = date.getFullYear();
    console.log(this.currentYear);
    this.signupForm = formBuilder.group({
      firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      address: [''],
      dob: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Register');
  }

  async signup() {
    this.loader = this.loadingCtrl.create({
      content: "Signing Up...",
      dismissOnPageChange: true
    });
    this.submitAttempt = true;
    let age = this.currentYear - this.signupForm.value.dob.substring(0, 4);
    console.log(this.signupForm.value.dob.substring(0, 4));
    if (!this.signupForm.valid || this.signupForm.value.password !== this.signupForm.value.confirmPassword || age < 18) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      this.loader.present();
      console.log('success!');
      console.log(this.signupForm.value);
      try {
        const newUser = await this.auth.signupUser(
          this.signupForm.value.email,
          this.signupForm.value.password,
          this.signupForm.value);
        this.navCtrl.setRoot('Tabs');
        // const token = await this.ps.initializePushToken(newUser.uid);
        // this.fs.updatePushToken(token);
      } catch (error) {
        this.signupAlert(error);
      }

    }
  }

  signupAlert(error) {
    this.loader.dismiss();
    let alert = this.alertCtrl.create({
      title: "Signup error",
      subTitle: error,
      buttons: ["OK"]
    })
    alert.present();
  }
}
