import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { GlobalVariable } from "../../app/global";
import { FirebaseService } from "../../providers/firebase-service";
import { NativeStorage } from "@ionic-native/native-storage";
declare var google;

@IonicPage()
@Component({
  selector: 'page-address-list',
  templateUrl: 'address-list.html',
})
export class AddressList implements OnInit {

  element: HTMLElement;
  autocompleteItems: any;
  autocomplete: any;
  nickname: string;
  item: any;
  addressDetails: Array<{
    formatted_address: string,
    name: string,
    nickname: string,
    placeId: string,
    lat: number,
    lng: number,
  }> = []

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private globals: GlobalVariable,
    private nativeStorage: NativeStorage,
    private fs: FirebaseService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddressList');
  }
  ngOnInit() {
    this.element = document.getElementById('map');
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
    this.nativeStorage.getItem("addresses")
      .then(
      (data) => this.addressDetails = data.addresses
      )
      .catch(
      () => this.addressDetails = []
      )
  }

  search() {
    console.log("autocomplete query: ", this.autocomplete.query);
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    console.log(this.globals.lat);
    var pyrmont = new google.maps.LatLng(this.globals.lat, this.globals.lng);
    let request = {
      location: pyrmont,
      radius: '5000',
      query: this.autocomplete.query,
    }
    let self = this;
    let service = new google.maps.places.PlacesService(this.element);
    service.textSearch(request, (results, status) => {
      console.log(status);
      if (status == 'OK') {
        this.autocompleteItems = results;

      }
      console.log(results);
    })
  }

  chooseItem(item) {
    // this.searchAttempt = true;
    this.item = item;
    this.autocompleteItems = [];
    this.autocomplete.query = item.name + " " + item.formatted_address;
    console.log(item);
    let addressDetails = {}

  }

  removeAddress(index: number) {
    this.addressDetails.splice(index, 1);
    console.log(this.addressDetails);
  }

  addAddress() {
    let item = this.item;
    this.addressDetails.push(
      {
        formatted_address: item.formatted_address,
        name: item.name,
        nickname: this.nickname,
        placeId: item.place_id,
        lat: item.geometry.location.lat(),
        lng: item.geometry.location.lng()
      }
    );
    this.item = '';
    this.nickname = '';
    this.autocomplete.query = '';
    // await this.fs.addAddress(this.addressDetails);
    // this.nativeStorage.setItem("addresses", { addresses: this.addressDetails });
    // this.viewCtrl.dismiss();
  }

  async dismiss() {
    if (this.addressDetails) {

      await this.fs.addAddress(this.addressDetails);
      this.nativeStorage.setItem("addresses", { addresses: this.addressDetails });
      this.viewCtrl.dismiss();
    }
  }

}
