import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddressList } from './address-list';

@NgModule({
  declarations: [
    AddressList,
  ],
  imports: [
    IonicPageModule.forChild(AddressList),
  ],
  exports: [
    AddressList
  ]
})
export class AddressListModule {}
