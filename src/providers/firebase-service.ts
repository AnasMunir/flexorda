import { Injectable } from '@angular/core';
// import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { Geolocation } from '@ionic-native/geolocation';
// import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { FirebaseListObservable, FirebaseObjectObservable, AngularFireDatabase } from "angularfire2/database";
import firebase from 'firebase';

declare var GeoFire: any;
@Injectable()
export class FirebaseService {

  orderList$: FirebaseListObservable<any>;
  orderObject$: FirebaseObjectObservable<any>;
  buyerObject$: FirebaseObjectObservable<any>;
  buyerAddress: any; buyerEmail: any;
  userData: any; userOrders: any;
  curentUser = firebase.auth().currentUser;
  orders = [];
  lat: number; lng: number;
  orderLocations = firebase.database().ref('orders/locations');
  userLocations = firebase.database().ref('users/locations');
  time: any;

  constructor(public af: AngularFireDatabase, public geolocation: Geolocation) {
    console.log('Hello FirebaseService Provider');
    this.orderList$ = af.list('/orders');

    this.orderObject$ = af.object('orders/');
    this.userData = firebase.database().ref('/users/' + this.curentUser.uid);
    this.userOrders = firebase.database().ref('/users/' + this.curentUser.uid + '/orders/');

    this.buyerObject$ = af.object('/users/' + this.curentUser.uid + '/', { preserveSnapshot: true });
    this.buyerObject$.subscribe(snapshot => {
      this.buyerAddress = snapshot.val().address
    })
  }

  storePictures(uid, name: string, photoRef)/*: firebase.Promise<any>*/ {
    let storageRef = firebase.storage().ref('/users/');
    return storageRef.child(uid).child(name)
      .putString(photoRef, 'base64', { contentType: 'image/png' })
    // .then(savedPicture => {
    //   this.userData.child(uid).update({
    //     [name]: savedPicture.downloadURL
    //   })
    // });
  }

  async addOrder(totalEstimate, shopContent, deliveryPlace, dropoffDetails, products): Promise<any> {
    if (dropoffDetails.anyTime) {
      this.time = { anyTime: dropoffDetails.anyTime };
    } else {
      this.time = { betweenTimeOne: dropoffDetails.betweenTimeOne, betweenTimeTwo: dropoffDetails.betweenTimeTwo };
    }

    try {
      const orderRef = await this.orderList$.push({
        status: 'active',
        cost: totalEstimate,
        reward: dropoffDetails.reward,
        delivery_time: this.time,
        buyer: {
          buyer_id: this.curentUser.uid,
          buyer_address: this.buyerAddress
        },
        shop: {
          shop_name: shopContent.name,
          address: shopContent.formatted_address,
          place_id: shopContent.placeId,
          lat: shopContent.lat,
          lng: shopContent.lng
        },
        location: {
          lat: shopContent.lat,
          lng: shopContent.lng
        },
        delivery_address: {
          name: deliveryPlace.name,
          address: deliveryPlace.formatted_address,
          place_id: deliveryPlace.placeId,
          lat: deliveryPlace.lat,
          lng: deliveryPlace.lng
        },
        products: products
      })
      let ref = firebase.database().ref('orders/' + orderRef.key + "/");
      ref.update({ uid: orderRef.key });
      let geoFire = new GeoFire(this.orderLocations);
      geoFire.set(orderRef.key, [shopContent.lat, shopContent.lng])
        .catch(error => {
          throw (error);
        });

      this.userOrders.child(orderRef.key).update({
        uid: orderRef.key,
        status: 'active',
        cost: totalEstimate,
        reward: dropoffDetails.reward,
        delivery_time: this.time,
        shop: {
          shop_name: shopContent.name,
          address: shopContent.formatted_address,
          place_id: shopContent.placeId,
          lat: shopContent.lat,
          lng: shopContent.lng
        },
        location: {
          lat: shopContent.lat,
          lng: shopContent.lng
        },
        delivery_address: {
          name: deliveryPlace.name,
          address: deliveryPlace.formatted_address,
          place_id: deliveryPlace.placeId,
          lat: deliveryPlace.lat,
          lng: deliveryPlace.lng
        },
        products: products
      })
    } catch (error) {
      console.error("error adding order:", error);
    }

  }

  editOrderStatus(orderKey) {
    let orderRef = firebase.database().ref('/orders/' + orderKey);
    return orderRef.update({
      status: 'inactive'
    }, (error) => {
      console.log('Error updating status: ' + error);
    })
  }
  async deleteOrder(orderKey) {
    let ref = firebase.database().ref('/users/' + this.curentUser.uid + '/orders/' + orderKey);
    let orderRef = firebase.database().ref('/orders/' + orderKey);
    await ref.remove();
    await orderRef.remove();
  }

  getUserOrders() {
    // let userOrders$: FirebaseListObservable<any>;
    // userOrders$ = this.af.database.list('/users/' + this.curentUser.uid + '/orders/');
    return this.af.list('/users/' + this.curentUser.uid + '/orders/');
  }

  getUserDeliveries() {
    return this.af.list('/users/' + this.curentUser.uid + '/deliveries/')
  }
  getSpecificOrder(orderUID) {
    // return this.af.database.list('/users/' + this.curentUser.uid + '/orders/'+ orderUID);
    let ref = firebase.database().ref('/users/' + this.curentUser.uid + '/orders/' + orderUID);
    return ref.once("value");
  }

  updateOrderStatus(orderUID, status: string): firebase.Promise<any> {
    const orderObject$ = this.af.object('orders/' + orderUID);
    return orderObject$.update({ status: status });
  }

  updateOrderStatusInBuyer(buyerUID, orderUID, status: string) {
    let ref = firebase.database().ref('/users/' + buyerUID + '/orders/' + orderUID);
    return ref.update({ status: status })
  }

  addDeliveriesInCurrentUser(buyerUID, orderUID, item) {
    var time;
    console.log(item);
    if (item.delivery_time.anyTime) {
      time = { anyTime: item.delivery_time.anyTime };
    } else {
      time = { betweenTimeOne: item.delivery_time.betweenTimeOne, betweenTimeTwo: item.delivery_time.betweenTimeTwo };
    }
    let ref = firebase.database().ref('/users/' + this.curentUser.uid);
    ref.child('deliveries').push({
      buyer_id: buyerUID,
      order_id: orderUID,
      cost: item.cost,
      reward: item.reward,
      delivery_time: time,
      shop: {
        shop_name: item.shop.shop_name,
        address: item.shop.address,
        place_id: item.shop.place_id,
        let: item.shop.lat,
        lng: item.shop.lng
      },
      delivery_address: {
        name: item.delivery_address.name,
        address: item.delivery_address.address,
        place_id: item.delivery_address.place_id,
        lat: item.delivery_address.lat,
        lng: item.delivery_address.lng
      },
      products: item.products
    })
  }

  addAddress(addresses): firebase.Promise<any> {
    let ref = firebase.database().ref('/users/' + this.curentUser.uid);
    return ref.update({
      addresses: addresses
    });
  }

  watchUserLocation() {

    setInterval(() => {
      this.getCurrentPosition()
    }, 5000);
  }
  // 35238899
  getCurrentPosition() {

    return this.geolocation.getCurrentPosition()
  }

  setUserLocation(lat, lng) {
    let geoFire = new GeoFire(this.userLocations);
    return geoFire.set(this.curentUser.uid, [lat, lng])
  }

  editOrder(orderKey, editData, products, deliveryTimeType): firebase.Promise<any> {
    let ref = firebase.database().ref('/users/' + this.curentUser.uid + '/orders/' + orderKey);
    let orderRef = firebase.database().ref('/orders/' + orderKey);
    if (deliveryTimeType == 'anyTime') {
      this.editProducts(orderKey, products);
      orderRef.update({
        delivery_time: { anyTime: editData.deliveryTime },
        reward: editData.reward
      })
      return ref.update({
        delivery_time: { anyTime: editData.deliveryTime },
        reward: editData.reward
      })
    } else if (deliveryTimeType == 'betweenTimeOne') {
      this.editProducts(orderKey, products);
      orderRef.update({
        delivery_time: { betweenTimeOne: editData.betweenTimeOne, betweenTimeTwo: editData.betweenTimeTwo },
        reward: editData.reward
      })
      return ref.update({
        delivery_time: { betweenTimeOne: editData.betweenTimeOne, betweenTimeTwo: editData.betweenTimeTwo },
        reward: editData.reward
      })
    }

  }

  editProducts(orderKey, products) {
    products.map((item, i) => {
      console.log(item);
      let userProductRef = firebase.database().ref('/users/' + this.curentUser.uid + '/orders/' + orderKey + '/products/' + i);
      let orderProductRef = firebase.database().ref('/orders/' + orderKey + '/products/' + i);
      userProductRef.update({
        cost: item.cost,
        name: item.name
      });
      orderProductRef.update({
        cost: item.cost,
        name: item.name
      });
    });
  }

  updatePushToken(pushToken) {
   this.userData.update({
    pushToken: pushToken
   }) 
  }
}