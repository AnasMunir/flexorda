import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Push, PushToken } from '@ionic/cloud-angular';

@Injectable()
export class PushService {

  constructor(public http: Http, private push: Push,) {
    console.log('Hello PushService Provider');
  }

  async initializePushToken(userUid) {
    const token: PushToken = await this.push.register()
    this.push.saveToken(token);
    return token.token;
    /*.then((t: PushToken) => {
      return this.push.saveToken(t);
    })*/
    /*.then((t: PushToken) => {
      console.log('Token saved:', t.token);
    });*/
  }

  sendPush() {
    // let msg = 'Your verification code: ' + randomCode;
    let url = 'https://api.ionic.io/push/notifications';
    // let body = new URLSearchParams();
    // body.set('originator', 'Verify');
    // body.set('body', msg);
    // body.set('recipients', phoneNumber);

    let headers = new Headers({
      'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxYmNkYmVlYy0wODA0LTQ3OGQtYWY0ZC04MDkyY2ZiMmUzYmQifQ.lPo-7Jcz2ag3seI_dakeM51RBAnsTEPTCiFNwcotYmc',
      'Content-Type': 'application/json'
    });
    // let options = new RequestOptions({ headers: headers });

    return this.http.get(url)
      .toPromise()
      .then(this.extractData)
      .then(this.logResponse)
      .catch(this.catchError);
  }

  private logResponse(res: Response) {
    console.log(res.json());
  }

  private extractData(res: Response) {
    return res.json();
  }

  private catchError(error: Response | any): Promise<any> {
    // console.log(error);
    // return Observable.throw(error.json().error || error || "some error in http get");
    return Promise.reject(error.json().error || error || "some error in http post");
  }

}
