import { Injectable } from '@angular/core';
// import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { GlobalVariable } from '../app/global';
import firebase from 'firebase';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  public fireAuth: any;
  public userData: any;

  constructor(private afAuth: AngularFireAuth, private globals: GlobalVariable) {
    console.log('Hello AuthService Provider');
    afAuth.authState.subscribe(user => {
      if (user) {
        this.fireAuth = user
        console.log(user);
      }
    });
    // this.fireAuth = firebase.auth();

    this.userData = firebase.database().ref('/users');
  }

  loginUser(email: string, password: string)/*: firebase.Promise<any>*/ {
    // return firebase.auth().signInWithEmailAndPassword(email, password);
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  signupUser(email: string, password: string, signupData: any)/*: firebase.Promise<any>*/ {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((newUser) => {
        this.globals.current_userUID = newUser.uid;
        this.userData.child(newUser.uid).set({
          email: email,
          first_name: signupData.firstName,
          last_name: signupData.lastName,
          date_of_birth: signupData.dob,
          address: signupData.address,
        });
      });
  }

  resetPassword(email: string): any {
    return this.fireAuth.sendPasswordResetEmail(email);
  }

  logoutUser()/*: firebase.Promise<any>*/ {
    return this.afAuth.auth.signOut();
  }

}
